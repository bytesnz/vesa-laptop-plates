# Laptop VESA Plates

Plates for holding a laptop between a screen and a VESA wall mount.

Wall plates (wall_plate.FCStd) attaches to a 100mm VESA mount. The screen
plates (screen_plate.FCStd) attaches to the wall plates with plastic spacers
leaving room for the laptop. The screen VESA mounting plate then attaches to
the screen plates.

![Assembly image showing plates assembled with spacers and laptop shape in between](./assembly.png)
